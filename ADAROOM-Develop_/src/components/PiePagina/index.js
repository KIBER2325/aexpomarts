import React from "react";
import facebook from "../../images/facebook.png";
import {
  PiePagEstilo,
  NombreAdaroom,
  DerechosA,
  Email,
  DirEmail,
  ImagenFacebook,
  WrapperA,
  WrapperB,
  Cont,
} from "./PiePagina.styles";

function PiePagina() {
  return (
    <PiePagEstilo>
      <WrapperA>
        <NombreAdaroom>K-M</NombreAdaroom>
        <DerechosA>© 2022 K-M. Todos los derechos reservados.</DerechosA>
      </WrapperA>
      <WrapperB>
        <Cont>
          <Email>E-mail:</Email>
          <DirEmail>K-M@gmail.com</DirEmail>
        </Cont>
        <a
          title="Facebook"
          href="https://www.facebook.com/search/top?q=udemy"
        >
          <ImagenFacebook src={facebook} alt="Facebook" />
        </a>
      </WrapperB>
    </PiePagEstilo>
  );
}
export default PiePagina;
